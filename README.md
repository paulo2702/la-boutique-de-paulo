 #WEB PROJECT: La boutique de Paulo#



##DESCRIPTION:

Symfony 5 E-Commerce website




##INSTALLATION:


###Clone the repository in Visual Studio Code or download archive

 git clone https://gitlab.com/paulo2702/la-boutique-de-paulo.git    

###Switch to the repo folder

    cd la-boutique-de-paulo in Visual Studio Code
    
###Install all the dependencies using composer

    composer install

###Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env
    
###Run the database migrations (Set the database connection in .env before migrating)

    symfony console doctrine:database:create

    symfony console make:migration

    symfony console doctrine:migrations:migrate

###Open boutique.sql file and execute it in your database manager

    
###Start the local development server

    symfony server:start 

###Environment variables

    .env - Environment variables can be set in this file
    
Note : You can quickly set the database information and other variables in this file and have the web site fully working.

###Todo

    -Legal notice template

    -Who are we?  template

    -General Data Protection Regulation 
