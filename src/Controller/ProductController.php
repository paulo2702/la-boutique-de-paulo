<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Classe\Search;
use App\Entity\Product;
use App\Form\SearchType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    private $entityManager;

    public function __construct( EntityManagerInterface $entityManager)
    {
        return $this->entityManager=$entityManager;
    }


    /**
     * @Route("/nos-produits", name="products")
     */
    public function index(Cart $cart,Request $request): Response
    {
        $products=$this->entityManager->getRepository(Product::class)->findAll();

        $search=new Search();
        $form=$this->createForm(SearchType::class,$search);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $products=$this->entityManager->getRepository(Product::class)->findBySearch($search);
        }
        else
        {
            $products=$this->entityManager->getRepository(Product::class)->findAll();   
        }

        return $this->render('product/index.html.twig',[
            
            'products'=>$products,
            'form'=>$form->createView(),
            'cart'=>$cart->getFull()
        ]);
    }

     /**
     * @Route("/produit/{slug}", name="product")
     */
    public function show(Cart $cart,$slug): Response
    {
        $product=$this->entityManager->getRepository(Product::class)->findOneBy(['slug' => $slug]);
        if(!$product){
            return $this->redirectToRoute('products');
        }
        return $this->render('product/show.html.twig',[  
            'product'=>$product,
            'cart'=>$cart->getFull()
        ]);
    }
}
