<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\OrderDetails;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class OrderSuccessController extends AbstractController
{
    private $entityManager;

    public function __construct( EntityManagerInterface $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    /**
     * @Route("/commande/merci/{stripeSessionId}", name="order_success")
     */
    public function index(MailerInterface $mailer,Cart $cart,$stripeSessionId): Response
    {
        $order=$this->entityManager->getRepository(Order::class)->findOneBy(['stripeSessionId'=>$stripeSessionId]);
        if(!$order || $order->getUser() != $this->getUser())
        {
            return $this->redirectToRoute('home');
        }

        if($order->getState()==0)
        {
            //Vider le panier
            $cart->remove();
            //Modifier le statut de la commande
            $order->setState(1);
            //Mise a jour du stock produit
            $orderDetails=$this->entityManager->getRepository(OrderDetails::class)->findBy(['myOrder'=>$order->getId()]);
            //dd($orderDetails);
            foreach($orderDetails as $data)
            {
                $products=$this->entityManager->getRepository(Product::class)->findBy(['name'=>$data->getProduct()]);
                foreach($products as $product)
                {
                    $quantity=$data->getQuantity();
                    $stock=$product->getStockQuantity();
                    $stock=$stock-$quantity;
                    $product->setStockQuantity($stock);
                    $this->entityManager->flush();
                }  
            } 
            $this->entityManager->flush();
             
            //Email de confirmation de commande
            $email = (new TemplatedEmail())
                ->from('laboutiquedepaulo@toto.fr')
                ->to($order->getUser()->getEmail())
                ->subject('Confirmation de commande.')
                ->htmlTemplate('email/order_confirmation.html.twig')
                ->context([
                    'order'=>$order
                ]);
            $mailer->send($email);       
        }
        return $this->render('order_success/index.html.twig',[
            'order'=>$order,
            'cart'=>$cart->getFull()
        ]);
    }
}
