<?php

namespace App\Controller;


use App\Classe\Cart;
use App\Entity\User;

use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    /**
     * @Route("/inscription", name="register")
     */
    public function index(Cart $cart,MailerInterface $mailer,Request $request, UserPasswordEncoderInterface $encoder): Response
    {
       
        $user = new User();
        $form = $this->createForm(RegisterType::class , $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) 
        {
            
           
            $user=$form->getData();
            $search_email = $this->entityManager->getRepository(User::class)->findOneBy(['email'=>$user->getEmail()]);

            if (!$search_email) 
            {
                $password = $encoder->encodePassword($user,$user->getPassword());
                $user->setPassword($password);
                $this->entityManager->persist($user);
                $this->entityManager->flush();
               
                $email = (new TemplatedEmail())
                ->from('laboutiquedepaulo@toto.fr')
                ->to($user->getEmail())
                ->subject('Bienvenue sur la boutique de Paulo.')
                ->htmlTemplate('email/welcome.html.twig')
                ->context([
                    'user'=>$user->getFullName()
                ]);
                $mailer->send($email);
                $this->addFlash('success', 'Votre compte a bien été créé ! , vous pouvez maintenant vous connecter au site.');
            }
            else
            {
                $this->addFlash('error', 'Cette adresse Email est déjà utilisée.');
            }      
        }  

        return $this->render('register/index.html.twig',[
            'form'=> $form->createView(),
            'cart'=>$cart->getFull()
        ]);
    }
}
