<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Form\ContactType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    /** 
     * @Route("/nous-contacter", name="contact")
     */
    public function index(Cart $cart,MailerInterface $mailer,Request $request): Response
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('notice', 'Merci de nous avoir contacté. Notre équipe va vous répondre dans les meilleurs délais.');
            $contact=$form->getData();
           
            $email = (new TemplatedEmail())
                ->from($contact['email'])
                ->to('laboutiquedepaulo@toto.fr')
                ->subject($contact['sujet'])
                ->htmlTemplate('email/contact.html.twig')
                ->context([
                    'contact'=>$contact
                ]);
            $mailer->send($email);
        }

        return $this->render('contact/index.html.twig', [
            'form' => $form->createView(),
            'cart'=>$cart->getFull()
        ]);
    }
}
